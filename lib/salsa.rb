require "gitlab"

begin
  load(File.expand_path(ENV['HOME']+"/.devscripts"))
rescue => error
  STDERR.puts "E: reading #{File.expand_path(ENV['HOME']+'/.devscripts')}"
  STDERR.puts error.inspect
end

salsarc=File.expand_path(File.dirname(__FILE__)+"/../salsarc")
load(salsarc) if File.exists?(salsarc)
#SALSA_TOKEN = ENV["GITLAB_API_PRIVATE_TOKEN"] || (File.exists?("salsa.token") && File.read("salsa.token").strip)
SALSA_NAMESPACE=2159 # ruby-team group
SALSA_GROUP="ruby-team"
SALSA_URL="https://salsa.debian.org/api/v4"
SALSA_CI_CONFIG_PATH="debian/salsa-ci.yml"

unless defined?(SALSA_TOKEN) && SALSA_TOKEN
  STDERR.puts "E: SALSA_TOKEN undefined."
  puts "I: Add your token to ~/.devscripts or salsarc"
  exit(1)
end

Gitlab.configure do |c|
  c.endpoint      = SALSA_URL
  c.private_token = SALSA_TOKEN
end

def create_project(name, ns)
  begin
    project=Gitlab.create_project(name, {ci_config_path: SALSA_CI_CONFIG_PATH,
                                         description: "#{name} packaging",
                                         namespace_id: ns == 'mentors' ? 7436 : SALSA_NAMESPACE,
                                         visibility: "public",
                                         wiki_access_level: "disabled",
                                         snippets_access_level: "disabled"})
    puts "I #{name}: repository created (id: #{project.id})"
  rescue Gitlab::Error::Error => error
    puts error
    STDERR.puts "E #{name}: repository creation failed. Probably already exists."
    project=get_project(name)
    exit(1) unless project
  end
  return project
end

def set_webhooks(project)
  set_tagpending(project)
  set_kgb(project)
end

def set_tagpending(project)
  #tagpending
  tagpending_url="https://webhook.salsa.debian.org/tagpending/#{project.name}"
  if Gitlab.project_hooks(project.id).all? {|hook| hook.url != tagpending_url}
    Gitlab.add_project_hook(project.id, tagpending_url, {push_events: true})
    puts "I #{project.name}: tagpending webhook activated"
  else
    puts "W #{project.name}: tagpending webhook already enabled"
  end
end

def set_kgb(project)
  # KGB irc notifications
  kgb_url="http://kgb.debian.net:9418/webhook/?channel=debian-ruby-changes;pipeline_only_status=failed;pipeline_only_status=success"
  if Gitlab.project_hooks(project.id).all? {|hook| hook.url != kgb_url}
    Gitlab.add_project_hook(project.id, kgb_url,
                            {issues_events: true,
                             push_events: true,
                             merge_requests_events: true,
                             tag_push_events: true,
                             note_events: true,
                             #job_events: true, # disable noise from salsa-ci test builds
                             pipeline_events: true,
                             wiki_page_events: true})
    puts "I #{project.name}: kgb webhook activated"
  else
    puts "W #{project.name}: kgb webhook already enabled"
  end
end

def each_team_project
  Gitlab.group_projects(SALSA_NAMESPACE, per_page: 100).auto_paginate do |project|
    yield(project)
  end
end

def get_project(name)
  begin
    project=Gitlab.project([SALSA_GROUP,name].join("/"))
    puts "I #{name}: project found (id #{project.id})."
    return project
  rescue Gitlab::Error::Error => error
    STDERR.puts error
    STDERR.puts "E #{name}: project not found."
    return nil
  end
end

def archive_project(project)
  unless Gitlab.respond_to?(:archive_project)
    STDERR.puts "E #{project.name}: repository not archived."
    STDERR.puts "E #{project.name}: ruby-gitlab >= 4.12.0 is required to be able to archive the project."
    return
  end
  begin
    Gitlab.archive_project([SALSA_GROUP,project.name].join("/"))
    puts "I #{project.name}: repository archived."
  rescue Gitlab::Error::Error => error
    STDERR.puts error
    STDERR.puts "E #{project.name}: repository not archived."
  end
end

def mv_project_to_attic(project)
  begin
    retval=Gitlab.transfer_project([SALSA_GROUP,project.name].join("/"), [SALSA_GROUP,'attic'].join("/"))
    puts "I #{project.name}: repository moved to attic (new path: #{retval.path_with_namespace})."
  rescue Gitlab::Error::Error => error
    STDERR.puts error
    STDERR.puts "E #{project.name}: repository not moved to attic."
  end
end

# vim: set ts=2 sw=2 ai si et:
